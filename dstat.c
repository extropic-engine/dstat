#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/sysinfo.h>

char *smprintf(char *fmt, ...) {
  va_list fmtargs;
  char *buf = NULL;

  va_start(fmtargs, fmt);
  if (vasprintf(&buf, fmt, fmtargs) == -1) {
    fprintf(stderr, "malloc vasprintf\n");
    exit(1);
  }
  va_end(fmtargs);

  return buf;
}

char charge() {
  FILE *fd;
  fd = fopen("/sys/class/power_supply/AC/online", "r");
  if (fd == NULL) {
    return '?';
  }
  int is_charging;
  fscanf(fd, "%d", &is_charging);
  fclose(fd);

  if (is_charging) {
    return '+';
  } else {
    return '-';
  }
}

float battery() {

  FILE *fd;
  int energy_now, energy_full, voltage_now;

  fd = fopen("/sys/class/power_supply/BAT0/energy_now", "r");
  if(fd == NULL) {
    fprintf(stderr, "Error opening energy_now.\n");
    return -1;
  }
  fscanf(fd, "%d", &energy_now);
  fclose(fd);


  fd = fopen("/sys/class/power_supply/BAT0/energy_full", "r");
  if(fd == NULL) {
    fprintf(stderr, "Error opening energy_full.\n");
    return -1;
  }
  fscanf(fd, "%d", &energy_full);
  fclose(fd);


  fd = fopen("/sys/class/power_supply/BAT0/voltage_now", "r");
  if(fd == NULL) {
    fprintf(stderr, "Error opening voltage_now.\n");
    return -1;
  }
  fscanf(fd, "%d", &voltage_now);
  fclose(fd);


  return ((float)energy_now * 1000 / (float)voltage_now) * 100 / ((float)energy_full * 1000 / (float)voltage_now);
}

char *loadavg(void) {
  double avgs[3];

  if (getloadavg(avgs, 3) < 0) {
    perror("getloadavg");
    exit(1);
  }

  return smprintf("%.2f %.2f %.2f", avgs[0], avgs[1], avgs[2]);
}

const int DAY = 24 * 3600;
const int HOUR = 3600;

char *up() {
  struct sysinfo info;
  int d,h,m = 0;
  sysinfo(&info);
  d = info.uptime / 86400;
  h = (info.uptime - (d * 86400)) / 3600;
  m = (info.uptime - ((d * 86400) + (h * 3600))) / 60;
  if (d > 0) {
    return smprintf("%dd %dh %dm",d,h,m);
  }
  if (h > 0) {
    return smprintf("%dh %dm",h,m);
  }
  return smprintf("%dm",m);
}

int main(void) {
  float bat = battery();
  char *uptm = up();
  char *avg = loadavg();
  printf("bat: %.lf%c | up: %s | cpu: %s", bat, charge(), uptm, avg);
  free(uptm);
  free(avg);

  return 0;
}

